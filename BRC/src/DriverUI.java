import java.sql.SQLException;
import java.util.Scanner;

public class DriverUI {
    public static void main(String args[]) {
        try {
            Interface.init();
            Scanner s = new Scanner(System.in);
            System.out.println("\nWhich interface?");
            System.out.println("1. Customer ordering");
            System.out.println("2. Member management");
            System.out.println("3. Manager ordering from vendors");

            switch (s.nextInt()) {
                case 1:
                    (new CustomerOrderingUI()).runInterface();
                    break;
                case 2:
                    (new MemberManagementUI()).runInterface();
                    break;
                case 3:
                    (new OrderingFromVendorsUI()).runInterface();
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
