import com.sun.istack.internal.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public abstract class Interface {
    private static int len = 25;
    private static double lenConstant = 1.1;
    private static String username = "kdf219";
    private static String password = "password";
    private static Connection connection;
    private static Scanner scanner;

    static String LOCATION_TABLE = "locations";
    static String CUSTOMER_TABLE = "customers";
    static String VENDOR_TABLE = "vendors";
    static String PRODUCT_TABLE = "products";
    static String LOCATION_INVENTORY_TABLE = "loc_inventory";
    static String CUSTOMER_ORDER_TABLE = "c_orders";
    static String PAY_TABLE = "pay_for";
    static String VENDOR_PAY_TABLE = "vendor_pay_for";
    static String PAYMENT_METHOD_TABLE = "payment_method";
    static String VENDOR_INVENTORY_TABLE = "v_inventory";
    static String VENDOR_ORDER_TABLE = "vendor_order";
    static String PERIODIC_TABLE = "periodic";

    private static ArrayList<String> inputs;

    abstract void runInterface() throws SQLException;


    static Scanner getScanner() {
        return scanner;
    }

    static void initTesting(String fileName) throws SQLException {
        System.out.println("IN TESTING MODE, READING INPUT FROM FILE");
        connection = DriverManager.getConnection("jdbc:oracle:thin:@edgar0.cse.lehigh.edu:1521:cse241",
                username, password);
        runStatement("commit work");
        System.out.println("successfully connected");
        try {
            scanner = new Scanner(new File("BRC/src/" + fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static void initTesting(String username, String password) throws SQLException {
        connection = DriverManager.getConnection("jdbc:oracle:thin:@edgar0.cse.lehigh.edu:1521:cse241",
                username, password);
        runStatement("commit work");
        scanner = new Scanner(System.in);
    }

    static void init() throws SQLException {
        scanner = new Scanner(System.in);
        System.out.print("Enter Oracle user id: ");
        username = scanner.nextLine();

        System.out.print("Enter Oracle password for " + username + ": ");
        password = scanner.nextLine();

        try {
            connection = DriverManager.getConnection("jdbc:oracle:thin:@edgar0.cse.lehigh.edu:1521:cse241",
                    username, password);
        } catch (SQLException e) {
            System.out.println("Invalid login. Try again");
            init();
        }
        runStatement("commit work");
    }

    static void runStatement(String stmt) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(stmt);
        preparedStatement.closeOnCompletion();
        preparedStatement.executeQuery();
//        System.out.println(stmt);
    }

    static ResultSet runStatementWithResultSet(String stmt) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(stmt);
        preparedStatement.closeOnCompletion();
        return preparedStatement.executeQuery();
    }

    static void viewAllInfo(String table) throws SQLException {
        viewInfo("*", table);
    }

    static void viewInfo(String select, String table) throws SQLException {
        viewInfo(select, table, "");
    }

    static void viewInfo(String select, String table, String other) throws SQLException {
        String stmt = "select " + select + " from " + table + " " + other;
        printMetaData(stmt);
        viewInfo(stmt);
    }

    static void viewInfo(String stmt) throws SQLException {
        ResultSet rs = runStatementWithResultSet(stmt);
//        int colCount = getMetaDataColumnCount(stmt);
//        int priceCol = getColumnNameIndex(stmt, "price");
//        while (rs.next()) {
//            for (int i = 1; i <= colCount; i++) {
//                if (i == priceCol) {
//                    System.out.format("%2.2f", rs.getDouble(i));
//                } else {
//                    System.out.format("%-" + (int) (len * lenConstant) + "." + (int) (len * lenConstant) + "s", rs.getString(i));
//                }
//                System.out.print(" ");
//            }
//            System.out.println();
//        }
        viewInfo(rs);
    }

    static boolean viewInfo(ResultSet rs) throws SQLException {
        int colCount = getMetaDataColumnCount(rs);
        int priceCol = getColumnNameIndex(rs, "price");
        if (!rs.next()) {
            return false;
        }
        do {
            for (int i = 1; i <= colCount; i++) {
                if (i == priceCol) {
                    System.out.format("%2.2f", rs.getDouble(i));
                } else {
                    System.out.format("%-" + (int) (len * lenConstant) + "." + (int) (len * lenConstant) + "s", rs.getString(i));
                }
                System.out.print(" ");
            }
            System.out.println();
        } while (rs.next());
        return true;
    }


    static int getMetaDataColumnCount(String stmt) throws SQLException {
        ResultSet rs = runStatementWithResultSet(stmt);
        ResultSetMetaData rsmd = rs.getMetaData();
        return rsmd.getColumnCount();
    }

    static int getMetaDataColumnCount(ResultSet rs) throws SQLException {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            return rsmd.getColumnCount();
        } catch (Exception e) {
            System.out.println("Caught");
            return 0;
        }
    }

    static ResultSetMetaData getMetaData(String stmt) throws SQLException {
        ResultSet rs = runStatementWithResultSet(stmt.trim());
        return rs.getMetaData();
    }

    static void printMetaData(String table) throws SQLException {
        ResultSetMetaData rsmd;
        if ((table.trim()).indexOf(' ') >= 0) {
            rsmd = getMetaData(table);
        } else {
            rsmd = getMetaData("select * from " + table);
        }
        int colCount = rsmd.getColumnCount();
        for (int i = 1; i <= colCount; i++) {
            System.out.format("%-" + len * lenConstant + "s", rsmd.getColumnName(i));
        }
        System.out.println();
        for (int i = 0; i < colCount * len; i++) {
            System.out.print("-");
        }
        System.out.println();

    }

    static void printMetaData(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int colCount = rsmd.getColumnCount();
        for (int i = 1; i <= colCount; i++) {
            System.out.format("%-" + len * lenConstant + "s", rsmd.getColumnName(i));
        }
        System.out.println();
        for (int i = 0; i < colCount * len; i++) {
            System.out.print("-");
        }
        System.out.println();

    }

    static int getColumnNameIndex(String table, String colName) throws SQLException {
        ResultSetMetaData rsmd = getMetaData(table);
        int colCount = rsmd.getColumnCount();
        for (int i = 1; i <= colCount; i++) {
            if (rsmd.getColumnLabel(i).equals(colName))
                return i;
        }
        return -1;
    }

    static int getColumnNameIndex(ResultSet rs, String colName) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int colCount = rsmd.getColumnCount();
        for (int i = 1; i <= colCount; i++) {
            if (rsmd.getColumnLabel(i).equals(colName))
                return i;
        }
        return -1;
    }

    public static Connection getConnection() {
        return connection;
    }

    static void updateColumn(String table, String variable, String newVal, String whereClause) throws SQLException {
        runStatement("update " + table + " set " + variable + " = \'" + newVal + "\'" +
                " where " + whereClause);
//        runStatement("commit work");
    }

    static void updateColumn(String table, String variable, int newVal, String whereClause) throws SQLException {
        updateColumn(table, variable, newVal + "", whereClause);
    }

    static void initMenu(@NotNull ArrayList<String> inputs) {
        if (Interface.inputs == null)
            inputs.add(0, "\n0: View Menu Again\n");
        Interface.inputs = inputs;
        System.out.println("\nMENU");
        for (int i = 0; i < 2; i++) {

            for (int j = 0; j < 40; j++) {
                System.out.print("_");
            }
            System.out.println();
        }

        for (String input : inputs) {
            System.out.println(input);
        }

        for (int i = 0; i < 40; i++) {
            System.out.print("_");
        }
        System.out.println();

    }

    static void menu() throws SQLException {
        initMenu(inputs);
    }

    static boolean checkExists(String table, String identifierVariable, String identifierValue,
                               String variable, String value) throws SQLException {
        ResultSet rs = runStatementWithResultSet("select * from " + table + " where " + identifierVariable +
                " = \'" + identifierValue + "\' and " + variable + " = \'" + value + "\'");
        return rs.next();
    }

    static boolean checkExists(String table, String variable, String value) throws SQLException {
        ResultSet rs = runStatementWithResultSet("select * from " + table + " where "
                + variable + " = \'" + value + "\'");
        return rs.next();
    }

    static boolean checkExists(String table, String variable, int value) throws SQLException {
        return checkExists(table, variable, value + "");
    }

    static void viewStoreInventory(int locId) throws SQLException {
        viewInfo("p_id \"Product Id\", brand, p_name \"Name\", " +
                        "p_size \"Size\", price, quantity", "" + PRODUCT_TABLE + " natural join " + LOCATION_INVENTORY_TABLE,
                "where loc_id = " + locId);
    }

}
