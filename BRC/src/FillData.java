import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class FillData {
    private static Connection connection = Interface.getConnection();
    private static String street, city, state, zip;
    private static String stmt;
    private static String filePath;
    private static BufferedReader in;
    static int i = 0;

    //TODO: Update fill methods for vendor stuff
    public static void main(String args[]) {

        try {
            Interface.initTesting("kdf219", "password");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<String> someTables = new ArrayList<>();
        someTables.add(Interface.VENDOR_ORDER_TABLE);
        someTables.add(Interface.VENDOR_PAY_TABLE);
        someTables.add(Interface.PERIODIC_TABLE);

//        dropTables(someTables);


//        dropAllTables();
        createAllTables();
        populateArrLists();
//        insertIntoLocations();
//        insertIntoCustomers();
//        insertIntoVendors();
//        insertIntoProducts(); //max cursors?
//        insertIntoLocInventory();
//        insertIntoCOrders(); //Need to change for changed o_id -- loc_id invalid identifier
//        insertIntoPayFor();
//        insertIntoPaymentMethod(); //Check constraint violated
//        insertIntoVInventory();
//        insertIntoVendorOrder();
//        insertIntoPeriodic();


    }

    private static void insertIntoLocations() {
        filePath = "BRC/src/LocationAddresses.txt";
        try {
            in = new BufferedReader(new FileReader(filePath));
            String line = in.readLine(); //reads first line -- if file is blank, will be null and will not enter while loop

            while (line != null) {
                String street, city, state, zip;

                street = line.substring(0, line.indexOf(','));
                line = line.substring(line.indexOf(',') + 1);
                line = line.trim();

                city = line.substring(0, line.indexOf(' '));
                line = line.substring(line.indexOf(' ') + 1);
                line = line.trim();

                state = line.substring(0, line.indexOf(' '));
                line = line.substring(line.indexOf(' ') + 1);
                line = line.trim();

                zip = line.trim();

                stmt = "INSERT INTO " + Interface.LOCATION_TABLE + " (" +
                        "street, city, state, zip) " +
                        "VALUES " +
                        "(\'" + street + "\',\'" + city +
                        "\',\'" + state + "\',\'" + zip + "\')";

                line = in.readLine(); //reads each line of the file
//            System.out.println(line);
                if (state.length() == 2) {
                    Interface.runStatement(stmt);
                }

            }

        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void insertIntoCustomers() {
        try {
            stmt = "select name from student";

            ResultSet rs = Interface.runStatementWithResultSet(stmt);
            ArrayList<String> fNames = new ArrayList<>();
            ArrayList<String> lNames = new ArrayList<>();
            ArrayList<String> streets = new ArrayList<>();
            ArrayList<String> cities = new ArrayList<>();
            ArrayList<String> states = new ArrayList<>();
            ArrayList<String> zips = new ArrayList<>();

            while (rs.next()) {
                fNames.add(rs.getString("name"));
            }

            rs = Interface.runStatementWithResultSet("select name from instructor");
            while (rs.next()) {
                lNames.add(rs.getString("name"));
            }

            filePath = "BRC/src/LocationAddresses.txt";
            try {
                in = new BufferedReader(new FileReader(filePath));
                String line = in.readLine(); //reads first line -- if file is blank, will be null and will not enter while loop

                while (line != null) {
                    String street, city, state, zip;

                    street = line.substring(0, line.indexOf(','));
                    line = line.substring(line.indexOf(',') + 1);
                    line = line.trim();

                    city = line.substring(0, line.indexOf(' '));
                    line = line.substring(line.indexOf(' ') + 1);
                    line = line.trim();

                    state = line.substring(0, line.indexOf(' '));
                    line = line.substring(line.indexOf(' ') + 1);
                    line = line.trim();

                    zip = line.trim();

                    streets.add(street);
                    cities.add(city);
                    states.add(state);
                    zips.add(zip);

                    line = in.readLine(); //reads each line of the file
                    if (state.length() == 2) {
                        Interface.runStatement(stmt);
                    }

                }

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }


            for (int j = 0; j < Math.min(Math.min(lNames.size(), fNames.size()), streets.size()); j++) {
                int k = r.nextInt(j + 1);
                stmt = "INSERT INTO " + Interface.CUSTOMER_TABLE + " (" +
                        "fname, lname, street, city, state, zip) " +
                        "VALUES " +
                        "(\'" + fNames.get(k) + "\',\'" + lNames.get(k) + "\'," +
                        "\'" + streets.get(k) + "\',\'" + cities.get(k) + "\',\'" +
                        states.get(k) + "\',\'" + zips.get(k) + "\')";


                System.out.println(stmt);

                Interface.runStatement(stmt);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void insertIntoVendors() {
        int i = 1;
        while (i <= 300) {
            stmt = "INSERT INTO vendors (" +
                    "v_id) " +
                    "VALUES " +
                    "(\'" + i + "\')";

            System.out.println(stmt);
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            i++;
        }
    }

    private static void insertIntoProducts() {
        String brandFilePath = "BRC/src/Brands.txt";
        String productFilePath = "BRC/src/Products.txt";
        try {
            in = new BufferedReader(new FileReader(brandFilePath));

            BufferedReader in2 = new BufferedReader(new FileReader(productFilePath));
            String brand = in.readLine(); //reads first line -- if file is blank, will be null and will not enter while loop
            String product = in2.readLine();
            int size;
            Random rn = new Random();
            while (product != null) {

                size = rn.nextInt(10) + 1;
                stmt = "INSERT INTO products (" +
                        "brand, p_name, p_size) " +
                        "VALUES " +
                        "(\'" + brand + "\',\'" + product +
                        "\',\'" + size + "\')";

                Interface.runStatement(stmt);
                brand = in.readLine();
                product = in2.readLine();

            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private static ArrayList<Integer> p_ids = new ArrayList<>();
    private static ArrayList<Integer> loc_ids = new ArrayList<>();
    private static ArrayList<Integer> o_ids = new ArrayList<>();
    private static ArrayList<Integer> c_ids = new ArrayList<>();
    private static ArrayList<Integer> v_ids = new ArrayList<>();
    private static ArrayList<Integer> vo_ids = new ArrayList<>();

    private static ArrayList<String> tableNames = new ArrayList<>();

    private static Random r = new Random();

    private static void populateArrLists() {
        try {
            Interface.runStatement("commit work");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        stmt = "SELECT p_id " +
                "FROM products";
//        PreparedStatement preparedStatement;// = connection.prepareStatement(stmt);
//        ResultSet rs = preparedStatement.executeQuery();
        ResultSet rs;
        try {
            rs = Interface.runStatementWithResultSet(stmt);

            while (rs.next()) {
                p_ids.add(rs.getInt("p_id"));
            }

            stmt = "SELECT loc_id " +
                    "FROM " + Interface.LOCATION_TABLE;
            rs = Interface.runStatementWithResultSet(stmt);
            while (rs.next()) {
                loc_ids.add(rs.getInt("loc_id"));
            }

            stmt = "SELECT o_id " +
                    "FROM c_orders";
            rs = Interface.runStatementWithResultSet(stmt);
            while (rs.next()) {
                o_ids.add(rs.getInt("o_id"));
            }

            stmt = "SELECT c_id " +
                    "FROM customers";
            rs = Interface.runStatementWithResultSet(stmt);
            while (rs.next()) {
                c_ids.add(rs.getInt("c_id"));
            }
            stmt = "SELECT v_id " +
                    "FROM vendors";
            rs = Interface.runStatementWithResultSet(stmt);
            while (rs.next()) {
                v_ids.add(rs.getInt("v_id"));
            }

            stmt = "SELECT vo_id " +
                    "FROM vendor_order";
            rs = Interface.runStatementWithResultSet(stmt);
            while (rs.next()) {
                vo_ids.add(rs.getInt("vo_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        tableNames.add(Interface.PERIODIC_TABLE);
        tableNames.add(Interface.VENDOR_ORDER_TABLE);
        tableNames.add(Interface.VENDOR_INVENTORY_TABLE);
        tableNames.add(Interface.PAYMENT_METHOD_TABLE);
        tableNames.add(Interface.PAY_TABLE);
        tableNames.add(Interface.CUSTOMER_ORDER_TABLE);
        tableNames.add(Interface.LOCATION_INVENTORY_TABLE);
        tableNames.add(Interface.PRODUCT_TABLE);
        tableNames.add(Interface.VENDOR_TABLE);
        tableNames.add(Interface.CUSTOMER_TABLE);
        tableNames.add(Interface.LOCATION_TABLE);
        tableNames.add(Interface.VENDOR_PAY_TABLE);
    }

    private static void insertIntoLocInventory() {
        while (true) {
            stmt = "INSERT into loc_inventory " +
                    "(p_id, loc_id, price, quantity) " +
                    "VALUES " +
                    "(\'" + p_ids.get(r.nextInt(p_ids.size() - 1)) +
                    "\', \'" + loc_ids.get(r.nextInt(loc_ids.size() - 1)) +
                    "\', \'" + r.nextInt(100) + "." + r.nextInt(100) +
                    "\', \'" + r.nextInt(500) +
                    "\')";
            System.out.println(stmt);
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertIntoCOrders() {
        while (true) {
            stmt = "INSERT INTO c_orders " +
                    "(p_id, loc_id, quantity) " +
                    "VALUES " +
                    "(\'" + p_ids.get(r.nextInt(p_ids.size() - 1)) +
                    "\', \'" + loc_ids.get(r.nextInt(loc_ids.size() - 1)) +
                    "\', \'" + r.nextInt(500) +
                    "\')";
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertIntoPayFor() {
        while (true) {
            stmt = "INSERT into pay_for " +
                    "(o_id, c_id, loc_id) " +
                    "VALUES " +
                    "(\'" + o_ids.get(r.nextInt(o_ids.size())) +
                    "\', \'" + c_ids.get(r.nextInt(c_ids.size())) +
//                    "\', \'300\', \'" +
//                    loc_ids.get(r.nextInt(loc_ids.size())) +
                    "\', \'" + loc_ids.get(r.nextInt(loc_ids.size())) +
                    "\')";
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertIntoPaymentMethod() {
        while (true) {
            String ccn = "NULL";
            String exp = "NULL";
            String cvv = "NULL";
            String check_num = "NULL";
            int method = r.nextInt(100);
            if (method < 65) { //credit card
                ccn = "\'";
                exp = "\'";
                cvv = "\'";
                for (int i = 0; i < 16; i++) {
                    ccn += r.nextInt(10);
                    if (i < 3)
                        cvv += r.nextInt(10);
                }
                exp += r.nextInt(2);
                exp += r.nextInt(10);
                exp += r.nextInt(10);
                exp += r.nextInt(10);
                ccn += "\'";
                exp += "\'";
                cvv += "\'";
            } else if (method < 90) { //cash
                // do nothing
            } else { //check
                check_num = r.nextInt(100000) + "";
            }
            stmt = "INSERT into payment_method " +
                    "(o_id, ccn, exp, cvv, check_num) " +
                    "VALUES " +
                    "(\'" + o_ids.get(r.nextInt(o_ids.size() - 1)) +
                    "\', " + ccn +
                    ", " + exp +
                    ", " + cvv +
                    ", " + check_num +
                    ")";
            System.out.println(stmt);
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertIntoVInventory() {
        while (true) {
            stmt = "INSERT INTO v_inventory" +
                    "(p_id, v_id, price, quantity) " +
                    "VALUES " +
                    "(\'" + p_ids.get(r.nextInt(p_ids.size() - 1)) +
                    "\', \'" + v_ids.get(r.nextInt(v_ids.size() - 1)) +
                    "\', \'" + r.nextInt(100) + "." + r.nextInt(100) +
                    "\', \'" + r.nextInt(500) +
                    "\')";
            System.out.println(stmt);
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertIntoVendorOrder() {
        while (true) {
            stmt = "INSERT INTO vendor_order " +
                    "(p_id, loc_id, order_date, v_id, price, quantity) " +
                    "VALUES " +
                    "(\'" + p_ids.get(r.nextInt(p_ids.size() - 1)) +
                    "\', \'" + loc_ids.get(r.nextInt(loc_ids.size() - 1)) +
                    "\', \'" + (r.nextInt(31) + 1) + "-MAY-" + (r.nextInt(18) + 1) + //date
                    "\', \'" + v_ids.get(r.nextInt(v_ids.size() - 1)) +
                    "\', \'" + r.nextInt(100) + "." + r.nextInt(100) +
                    "\', \'" + r.nextInt(500) +
                    "\')";
            System.out.println(stmt);
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void insertIntoPeriodic() {
        while (true) {
            stmt = "INSERT INTO periodic " +
                    "(vo_id, freq) " +
                    "VALUES " +
                    "(\'" + vo_ids.get(r.nextInt(vo_ids.size() - 1)) +
                    "\', \'" + (r.nextInt(12) + 1) +
                    "\')";
            System.out.println(stmt);
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void dropAllTables() {
        for (int i = 0; i < tableNames.size(); i++) {
            stmt = "drop table " + tableNames.get(i);
            System.out.println(stmt);
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
            }
        }
    }

    private static void dropTables(ArrayList<String> tablesToDrop) {
        for (int i = 0; i < tablesToDrop.size(); i++) {
            stmt = "drop table " + tablesToDrop.get(i);
            System.out.println(stmt);
            try {
                Interface.runStatement(stmt);
            } catch (SQLException e) {
            }
        }
    }

    private static void createAllTables() {
        filePath = "BRC/src/Tables.txt";
        Scanner fileReader = null;
        try {
            fileReader = new Scanner(new File(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String stmt;

        fileReader.useDelimiter(";");

        while (fileReader.hasNext()) {
            stmt = fileReader.next();
            try {
                Interface.runStatement(stmt);
            } catch (SQLSyntaxErrorException e) {
                stmt = stmt.substring(stmt.indexOf("TABLE") + ("TABLE").length() + 1);
                stmt = stmt.substring(0, stmt.indexOf(" "));
                System.out.print(stmt.toUpperCase());
                System.out.println(" already exists");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

}