import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class CustomerOrderingUI extends Interface {
    private static int locId = -1;
    private static Scanner scanner;
    private static double totalPrice = 0;
    private static int oId = -1;
    private static boolean member;
    private static int cId;
    private static int memberNumber;
    private static boolean firstItem = true;

    //TODO: Debug with updated price in customer order table
    void runInterface() throws SQLException {
//        initTesting("inputs2");
//        init(); // used when asking the user for login and password
        initTesting("kdf219", "password");
        scanner = getScanner();
        selectStore();

        ArrayList<String> inputs = new ArrayList<>();
        inputs.add("1: Add Item to Cart by ID Number");
        inputs.add("2: View Store Inventory");
        inputs.add("3: View Cart");
        inputs.add("4: Remove Item from Cart");
        inputs.add("\n-1: Checkout (quit)");

        initMenu(inputs);

        while (true) {
            selectFromMenu();
        }
    }

    static void selectStore() throws SQLException {
        int store = -1;

        while ((store != 1) && (store != 2)) {
            System.out.println("Are you shopping (1) online or (2) in store?");
            store = scanner.nextInt();
            scanner.nextLine();
        }

        System.out.println("Are you a member (y/n)?");
        member = false;
        if (((scanner.nextLine()).toLowerCase()).equals("y")) { //if they are a member
            long startTime = System.currentTimeMillis(); //fetch starting time
            do {
                System.out.println("Enter member number (no spaces or dashes)");
                cId = scanner.nextInt();
                scanner.nextLine();
                member = true;
            } while (!checkExists(CUSTOMER_TABLE, "c_id", cId) &&
                    (System.currentTimeMillis() - startTime) > 10000);
            //checking for timeout
            if ((System.currentTimeMillis() - startTime) > 10000) {
                member = false; //not a member if timed out
                cId = -1;
            } else {
                //TODO: Welcome member by name
            }
        } //otherwise is handled later in the code

        if (store == 2) { //shopping in store
            store = -1;
            boolean initial = true;
            while (!checkExists(LOCATION_TABLE, "loc_id", store + "")) {
                viewAllInfo(LOCATION_TABLE);
                if (initial)
                    System.out.println("Select a store (enter store ID):");
                else
                    System.out.println("Invalid store ID, try again:");
                store = scanner.nextInt();
                scanner.nextLine();
                initial = false;
            }
        }
        locId = store;
        if (locId < 1)
            selectStore();
    }


    //TODO: Refactor with interface method
    static void selectFromMenu() throws SQLException {
        menu();

        String choice = scanner.nextLine();
        int pId = -1;
        switch (choice) {
            case "1": //add item by id
                viewStoreInventory(locId);

                boolean initial = true;

                while (!checkExists(LOCATION_INVENTORY_TABLE, "p_id", pId + "")) {
                    if (!initial) {
                        System.out.print("Invalid Id. Try again. ");
                    } else {
                        initial = false;
                    }
                    System.out.print("Enter Product Id: ");
                    pId = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println();
                }

                System.out.print("Enter Quantity: ");
                int quantity = scanner.nextInt();
                scanner.nextLine();

                ResultSet rs = runStatementWithResultSet("select price, quantity from " + LOCATION_INVENTORY_TABLE + " where p_id = " + pId);
                rs.next();
                int newQuantity = rs.getInt("quantity") - quantity;

                while (newQuantity < 0) {
                    //Customer trying to buy more than what's available
                    System.out.println("Only have " + rs.getInt("quantity") + " in stock. Enter Quantity:");
                    quantity = scanner.nextInt();
                    scanner.nextLine();
                    rs = runStatementWithResultSet("select price, quantity from " + LOCATION_INVENTORY_TABLE + " where p_id = " + pId);
                    if (rs.next())
                        newQuantity = rs.getInt("quantity") - quantity;
                }

                //Adds to the running total price
                totalPrice += rs.getDouble("price") * quantity;

                //Alter column to reflect the new available quantity
                updateColumn(LOCATION_INVENTORY_TABLE, "quantity", newQuantity, "p_id = " + pId);

                if (oId < 0) {
                    //first item in this order

                    //Get the last order id so we can increment and go from there
                    rs = runStatementWithResultSet("select max(o_id) from " + PAY_TABLE);
                    rs.next(); //Gets the max oId
                    oId = rs.getInt(1);
                    oId++;

                    if (!member) {
                        //need to create a new customer in customer table
                        runStatement("insert into " + CUSTOMER_TABLE + " (c_id, fname)" +
                                " VALUES (DEFAULT, DEFAULT)");
                        rs = runStatementWithResultSet("select c_id from " + CUSTOMER_TABLE);
                        while (rs.next()) {
                            cId = rs.getInt(1);
                        }
                    }

                    if (firstItem) {
                        firstItem = false;
                        //inserting into the pay table
                        String stmt = "insert into " + PAY_TABLE + " (o_id, c_id, loc_id) " +
                                "VALUES (" + oId + ", " + cId + ", " + locId + ")";
                    }
                }

                // if p_id doesn't exist yet -- first time adding to the cart
                if (!(runStatementWithResultSet("select * from " + CUSTOMER_ORDER_TABLE + " where p_id = " +
                        pId + " and o_id = " + oId)).next()) {
                    rs = runStatementWithResultSet("select price from " + LOCATION_INVENTORY_TABLE +
                            " where loc_id = \'" + locId + "\' and p_id = \'" + pId + "\'");
                    double price;
                    if (rs.next()) {
                        price = rs.getDouble(1);
                    } else {
                        System.out.println("An error occurred. Please try again");
                        break;
                    }
                    runStatement("insert into " + CUSTOMER_ORDER_TABLE + " (o_id, p_id, quantity, price) " +
                            "VALUES (\'" + oId + "\', \'" + pId + "\', \'" + quantity + "\'," +
                            "\'" + price + "\')");
                } else {
                    rs = runStatementWithResultSet("select * from " + CUSTOMER_ORDER_TABLE + " " +
                            "where o_id = " + oId + " and p_id = " + pId);
                    rs.next();
                    //Updating quantity rather than inserting new item b/c already in cart
                    int oldQuantity = rs.getInt("quantity");
                    updateColumn(CUSTOMER_ORDER_TABLE, "quantity", oldQuantity + quantity,
                            "o_id = \'" + oId + "\' and p_id = \'" + pId + "\'");
                }
                viewCart();
                break;

            case "2": //view store's inventory
                System.out.println("Store Inventory:");
                viewStoreInventory(locId);
                break;

            case "3": //view cart
                viewCart();
                break;

            case "4": //remove items from cart
                viewCart();
                System.out.println("Enter the ID of the item you would like to remove?");
                int remId = scanner.nextInt();
                scanner.nextLine();

                while (!checkExists(CUSTOMER_ORDER_TABLE, "o_id", oId + "",
                        "p_id", remId + "")) {
                    //User tried to remove item that was not in their cart
                    System.out.println("Invalid ID. Check, is this item in your cart?");
                    System.out.println("Enter the ID of the item you would like to remove?");
                    remId = scanner.nextInt();
                    scanner.nextLine();
                }

                rs = runStatementWithResultSet("select price, quantity from " + LOCATION_INVENTORY_TABLE + " where p_id = " + remId);
                System.out.println("Enter quantity to remove (if entered more than in your cart they will all be removed):");
                int remQuantity = scanner.nextInt();
                if (rs.next()) {
                    newQuantity = rs.getInt("quantity") - remQuantity;
                    if (newQuantity < 0)
                        newQuantity = 0;

                    //Update cart with new quantity
                    updateColumn(LOCATION_INVENTORY_TABLE, "quantity", newQuantity, "where p_id = " + pId);
                    scanner.nextLine();
                }
                viewCart();
                break;

            case "-1":
                viewCart();

                System.out.print("Your total cost is $");
                System.out.format("%-10.2f\n", totalPrice);


                System.out.println("Choose a payment method: Credit/Debit Card (1), Cash (2), Check (3)");
                int paymentMethod = scanner.nextInt();
                scanner.nextLine();

                switch (paymentMethod) {
                    case 1:
                        System.out.print("Enter your CCN (no spaces, dashes, etc.): ");
                        String ccn = scanner.nextLine();

                        System.out.print("Enter expiration date (format MMYY): ");
                        int exp = scanner.nextInt();
                        scanner.nextLine();

                        System.out.print("Enter CVV (format XXX): ");
                        int cvv = scanner.nextInt();
                        scanner.nextLine();

                        runStatement("insert into " + PAYMENT_METHOD_TABLE + " (o_id, ccn, exp, cvv) " +
                                "VALUES (\'" + oId + "\', \'" + ccn + "\', \'" + exp + "\', " + cvv + "\')");

                        break;
                    case 2:

                        break;
                    case 3:
                        System.out.print("Enter check number: ");
                        int checkNum = scanner.nextInt();
                        scanner.nextLine();

                        runStatement("insert into " + PAYMENT_METHOD_TABLE + " (o_id, check_num) " +
                                "VALUES (\'" + oId + "\', \'" + checkNum + "\')");

                        break;
                }

                System.out.println("Thanks! Please come again!");

                System.exit(0);
                break;
            default:

                menu();
        }
        System.out.println();
    }

    static void viewCart() throws SQLException {
        System.out.println("\nCart:");

        viewInfo("p_name \"Product Name\", brand, quantity, price, p_id \"Product ID\"",
                "(" + CUSTOMER_ORDER_TABLE + " natural join " + PRODUCT_TABLE + ") natural join " +
                        "(select p_id, price, loc_id from " + LOCATION_INVENTORY_TABLE + ")",
                "where o_id = " + oId + " and o_id = " + oId + " and loc_id = " + locId);
    }


    public static void main(String[] args) {
        try {
            (new CustomerOrderingUI()).runInterface();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
