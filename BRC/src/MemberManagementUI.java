import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

//TODO: View order history

public class MemberManagementUI extends Interface {
    private static Scanner scanner;
    private static ResultSet rs = null;
    private static int cId = -1;
    private static boolean firstRun = true;

    //TODO: Change to non-testing
    @Override
    void runInterface() throws SQLException {
        System.out.println("1 for testing, 2 for testing w file");
        scanner = new Scanner(System.in);
//        switch (scanner.nextLine()) {
//            case "1":
//                initTesting("kdf219", "password");
//                break;
//            case "2":
//                initTesting("inputs.txt");
//                break;
//        }
//        init();
        scanner = getScanner();

        ArrayList<String> inputs = new ArrayList<>();
        inputs.add("1: View all members");
        inputs.add("2: Update member info");
        inputs.add("3: Lookup member (forgot ID)");
        inputs.add("4: View order history");
        inputs.add("5: View member information");
        inputs.add("6: Select user");
        inputs.add("7: View all members in custom order");
        inputs.add("\n-1: Quit");

        initMenu(inputs);

        while (true) {
            selectFromMenu();
        }
    }

    private void selectFromMenu() throws SQLException {
        if (!firstRun) {
            menu();
        } else {
            firstRun = false;
        }

        int selection = scanner.nextInt();
        scanner.nextLine();

        switch (selection) {
            //view all members
            case 1:
                viewAllMembers();
                break;

            //update member info
            case 2:
                updateMemberInfo();
                break;

            //lookup member by info
            case 3:
                ArrayList<String> variables = new ArrayList<>();
                ArrayList<String> values = new ArrayList<>();

                //TODO: Loop if not a single return
                //TODO: Default
                System.out.println("What is the user's first name (press enter with no input to skip)?");
                String fName = scanner.nextLine();
                if (fName.trim().length() > 0) {
                    variables.add("fname");
                    fName = getCaseSensitiveString(fName);
                    values.add(fName);
                }

                System.out.println("What is the user's last name (press enter with no input to skip)?");
                String lName = scanner.nextLine();
                if (lName.trim().length() > 0) {
                    variables.add("lname");
                    lName = getCaseSensitiveString(lName);
                    values.add(lName);
                }

                //handles zero results and sets cid if one result
                lookupUser(variables, values);
                if (cId > 0) {
                    //only break if a value was found, otherwise ask about address also
                    break;
                }
//                    case "address":
                //TODO: Deal with multiple words in street all needing first word capitalized
                //loop with method
                System.out.println("What is the user's address (press enter with no input to skip)?");
                String street = scanner.nextLine();
                if (street.trim().length() > 0) {
                    variables.add("street");
                    street = getCaseSensitiveString(street);
                    values.add(street);
                }

                System.out.println("What is the user's city (press enter with no input to skip)?");
                String city = scanner.nextLine();
                if (city.trim().length() > 0) {
                    variables.add("city");
                    city = getCaseSensitiveString(city);
                    values.add(city);
                }

                System.out.println("What is the user's state (press enter with no input to skip)?");
                String state = scanner.nextLine();
                if (city.trim().length() > 0) {
                    variables.add("state");
                    state = state.toUpperCase();
                    values.add(state);
                }

                System.out.println("What is the user's zip (press enter with no input to skip)?");
                String zip = scanner.nextLine();
                if (zip.trim().length() > 0) {
                    variables.add("zip");
                    values.add(zip);
                }
                //TODO: error if at this point there are multiple results returned
                lookupUser(variables, values);
                break;
            case 4:
                //view order history
                viewOrderHistory();
                break;
            case 5:
                selectUser();
                printMetaData(CUSTOMER_TABLE);
                viewInfo("select * from " + CUSTOMER_TABLE + " where c_id = \'" + cId + "\'");
                break;
            case 6:
                cId = -1;
                selectUser();
                break;
            case 7:
                printMetaData(CUSTOMER_TABLE);
                System.out.println("Which of the above would you like to order by?");
                String choice = scanner.nextLine();
                if (choice.length() < 1) {
                    choice = "c_id";
                }
                //does not break with invalid input
                printMetaData(CUSTOMER_TABLE);
                viewInfo("select * from " + CUSTOMER_TABLE + " order by " + choice);
                break;
            case -1:
                System.exit(0);
            default:
                menu();
        }

    }


    private static void addNewMember() throws SQLException {
        runStatement("insert into " + CUSTOMER_TABLE + " (c_id)" +
                " VALUES (DEFAULT)");
        rs = runStatementWithResultSet("select max(c_id) from " + CUSTOMER_TABLE);
        rs.next();
        cId = rs.getInt(1);

        updateName();
        updateAddress();
        runStatement("commit work");
    }

    private static void updateAddress() throws SQLException {
        updateStreet();
        updateCity();
        updateState();
        updateZip();
    }

    private static void updateName() throws SQLException {
        System.out.println("Enter your first name: ");
        String tempVar = scanner.nextLine();
        if (tempVar.trim().length() > 0) {
            tempVar = getCaseSensitiveString(tempVar);
            updateColumn(CUSTOMER_TABLE, "fname", tempVar, "c_id = " + cId);
        }

        System.out.println("Enter your last name: ");
        tempVar = scanner.nextLine();
        if (tempVar.trim().length() > 0) {
            tempVar = getCaseSensitiveString(tempVar);
            updateColumn(CUSTOMER_TABLE, "lname", tempVar, "c_id = " + cId);
        }
    }

    private static void updateStreet() throws SQLException {
        System.out.println("Enter your street name: ");
        String street = scanner.nextLine();
        street = getCaseSensitiveString(street);
        updateColumn(CUSTOMER_TABLE, "street", street, "c_id = " + cId);
    }

    private static void updateCity() throws SQLException {
        System.out.println("Enter your city: ");
        String city = scanner.nextLine();
        city = getCaseSensitiveString(city);
        updateColumn(CUSTOMER_TABLE, "city", city, "c_id = " + cId);
    }

    private static void updateState() throws SQLException {
        System.out.println("Enter your state (2 letters): ");
        String state = scanner.nextLine().toUpperCase();
        updateColumn(CUSTOMER_TABLE, "state", state, "c_id = " + cId);
    }

    private static void updateZip() throws SQLException {
        System.out.println("Enter your zip code: ");
        String zip = scanner.nextLine();
        updateColumn(CUSTOMER_TABLE, "zip", zip + "", "c_id = " + cId);
    }

    private static void viewAllMembers() throws SQLException {
        viewAllInfo(CUSTOMER_TABLE);
    }

    private static void updateMemberInfo() throws SQLException {
        boolean init = true;
        selectUser();
        System.out.println("Do you want to update your name (y/n)?");
        String ans = scanner.nextLine();
        ans = ans.toLowerCase().trim();
        if (ans.equals("y")) {
            updateName();
        }

        System.out.println("Do you want to update your address (y/n)?");
        ans = scanner.nextLine();
        ans = ans.toLowerCase().trim();
        if (ans.equals("y")) {
            updateAddress();
        }
    }

    private static void selectUser() throws SQLException {
        ArrayList<String> variables = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();
        if (cId < 0) {
            System.out.println("Do you know your customer id (y/n)?");
            if ((((scanner.nextLine()).toLowerCase()).trim()).equals("y")) {
                selectUser(true);
            } else {
                //Lookup by other info
                System.out.println("What is your last name?");
                String lname = scanner.nextLine().trim();
                lname = getCaseSensitiveString(lname);
                rs = runStatementWithResultSet("select count(*) from " + CUSTOMER_TABLE + " where lname like \'" +
                        lname + "%\'");
                if (!rs.next()) { //add as a new member if no results
                    addNewMember();
                    return;
                }

                variables.add("lname");
                lname = getCaseSensitiveString(lname);
                values.add(lname);
                //already called rs.next()
                if (rs.getInt(1) == 1) {
                    lookupUser(variables, values);
                } else if (rs.getInt(1) < 1) {
                    System.out.println("No members found. Redirecting to create a new membership.");
                    addNewMember();
                    return;
                } else {
                    System.out.print("Too many results. ");
                    System.out.println("What is your first name?");
                    String fname = scanner.nextLine().toLowerCase().trim();
                    lookupUser(variables, values);
                    if (!rs.next()) {
                        addNewMember();
                        return;
                    }
                    variables.add("fname");
                    fname = getCaseSensitiveString(fname);
                    values.add(fname);
                    if (rs.getInt(1) == 1) {
                        lookupUser(variables, values);
                    } else {
                        System.out.print("Too many results. ");
                        System.out.println("Enter your street");
                        String street = scanner.nextLine();

                        System.out.println("Enter your city");
                        String city = scanner.nextLine();

                        String state = "initializing state";
                        while (state.length() != 2 && state.length() != 0) {
                            System.out.println("Enter you state (2 letters)");
                            state = scanner.nextLine();
                        }

                        System.out.println("Enter your zip code");
                        String zip = scanner.nextLine();

                        variables.add("street");
                        variables.add("city");
                        variables.add("state");
                        variables.add("zip");

                        street = getCaseSensitiveString(street);
                        city = getCaseSensitiveString(city);
                        values.add(street);
                        values.add(city);
                        values.add(state);
                        values.add(zip);

                        lookupUser(variables, values);
                        if (!rs.next()) {
                            System.out.println("No members found. Redirecting to create a new membership.");
                            addNewMember();
                            return;
                        }
//                        cId = rs.getInt("c_id");
                    }
                }
            }
        }
        ResultSet rsTemp = runStatementWithResultSet("select fname from " + CUSTOMER_TABLE + " where c_id = " + cId);
        if (rsTemp.next()) {
            System.out.println("Welcome " + rsTemp.getString(1));
        }
    }

    private static void selectUser(boolean init) throws SQLException {
        while (!checkExists(CUSTOMER_TABLE, "c_id", cId)) {
            if (!init) {
                System.out.println("Invalid customer id, try again.");
            } else {
                init = false;
            }
            System.out.print("\nEnter your customer id: ");
            cId = scanner.nextInt();
            scanner.nextLine();
        }
        rs = runStatementWithResultSet("select * from " + CUSTOMER_TABLE + " where c_id = \'"
                + cId + "\'");
    }


    private static void lookupUser(ArrayList<String> variables, ArrayList<String> values) throws SQLException {
        lookupCustomerInfo(variables, values, CUSTOMER_TABLE);
    }

    private static void lookupCustomerInfo(ArrayList<String> variables, ArrayList<String> values, String table) throws SQLException {
        printMetaData(table);
        String startCount = "select count (*) ";
        String start = "select * ";
        String stmt = "from " + table + " where ";
        for (int i = 0; i < variables.size(); i++) {
            String correctCaseString = values.get(i).toLowerCase();
            correctCaseString = getCaseSensitiveString(correctCaseString);
            values.set(i, correctCaseString);
            stmt += variables.get(i) + " like ";
            stmt += "\'" + values.get(i) + "%\'";
            if (i < variables.size() - 1) {
                stmt += " AND ";
            }
        }

        rs = runStatementWithResultSet(startCount + stmt);
        rs.next();
        int numResults = rs.getInt(1);
        if (numResults < 1) {
            System.out.println("No users found. Redirecting to create a new membership.");
            addNewMember();
            return;
        }

        rs = runStatementWithResultSet(start + stmt);
        if (rs.next() && (numResults == 1)) {
            cId = rs.getInt("c_id");
        }

        rs = runStatementWithResultSet(start + stmt);
        if (numResults > 1 &&
                (variables.contains("street")) || variables.contains("state") ||
                variables.contains("city") || variables.contains("zip")) {
            //if returns too many options
            viewInfo(rs);
            System.out.println("Select a C_ID from the list above, or type \"new\" to add a new member");
            int choice;
            try {
                choice = scanner.nextInt();
                if (checkExists(CUSTOMER_TABLE, "c_id", choice)) {
                    cId = choice;
                } else {
                    throw new InputMismatchException();
                }
            } catch (InputMismatchException e) {
                addNewMember();
            }
        }
    }

    private static String getCaseSensitiveString(String originalString) {
        if (originalString.length() < 1)
            return originalString;

        String checkValidity = originalString;
        originalString = originalString.toLowerCase();
        char firstLetter = originalString.charAt(0);
        if (!Character.isLetter(firstLetter)) { //not a letter
            return checkValidity;
        }
        firstLetter -= ('a' - 'A'); // converts letter to uppercase
        originalString = originalString.substring(1);
        originalString = firstLetter + originalString;
        return originalString;
    }

    private static void lookupCustomerOrders(ArrayList<String> variables, ArrayList<String> values) throws SQLException {

        printMetaData(PAY_TABLE);
        String stmt = "select * from " + PAY_TABLE + " where ";
        for (int i = 0; i < variables.size(); i++) {
            stmt += variables.get(i) + " = ";
            stmt += "\'" + values.get(i) + "\'";
            if (i < variables.size() - 1) {
                stmt += " OR ";
            }
        }
        rs = runStatementWithResultSet(stmt);
    }

    private static void viewOrderHistory() throws SQLException {
        while (cId < 0) {
            selectUser();
        }

        String stmt = "select o_id from " + PAY_TABLE + " where c_id = \'" + cId + "\'";
        rs = runStatementWithResultSet(stmt);

        ArrayList<String> variables = new ArrayList<>();
        ArrayList<String> values = new ArrayList<>();

        if (rs.next()) {
            do {
                variables.add("o_id");
                values.add(rs.getString(1));
            } while (rs.next());
            lookupCustomerOrders(variables, values);
            viewInfo(rs);
        } else {
            System.out.println("No order history for user " + cId);
        }

    }


    public static void main(String[] args) {
        try {
            (new MemberManagementUI()).runInterface();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}