import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class OrderingFromVendorsUI extends Interface {
    private static Scanner scanner;
    private static boolean firstRun = true;
    private static boolean firstItem = true;
    private static int minQuantity = 0;
    private static int locId = -1;
    private static int pId = -1;
    private static int voId = -1;
    private static int vId = -1;
    private static double totalPrice = 0;
    private static ResultSet rs;

    @Override
    void runInterface() throws SQLException {
        System.out.println("1 for testing, 2 for testing w file");
        scanner = new Scanner(System.in);
//        switch (scanner.nextLine()) {
//            case "1":
//                initTesting("kdf219", "password");
//                break;
//            case "2":
//                initTesting("inputs.txt");
//                break;
//        }
////        init();
        scanner = getScanner();

        ArrayList<String> inputs = new ArrayList<>();
        inputs.add("1: Select or change your store");
        inputs.add("2: View inventory");
        inputs.add("3: Calibrate alerts");
        inputs.add("4: Make new vendor order");

        inputs.add("\n-1: Quit");

        initMenu(inputs);


        while (true)

        {
            selectFromMenu();
        }

    }

    private void selectFromMenu() throws SQLException {
        if (!firstRun) {
            menu();
        } else {
            firstRun = false;
        }

        int selection = scanner.nextInt();
        scanner.nextLine();

        switch (selection) {
            case 1:
                //select store
                locId = -1;
                selectStore();
                break;
            case 2:
                //view store inventory
                checkValidStore();
                viewStoreInventory(locId);
                break;
            case 3:
                //calibrate alerts
                checkValidStore();
                System.out.println("You are currently set to receive alerts when your inventory falls " +
                        "at or beneath " + minQuantity);
                setMinQuantity();
                break;
            case 4:
                //new vendor order
                checkValidStore();
                newVendorOrder();
                break;
            case -1:
                System.exit(0);
                break;
        }
    }

    private static void newVendorOrder() throws SQLException {
        checkValidStore();
        checkMinQuantity();
        viewInfo("*", PRODUCT_TABLE, "order by p_id");
        System.out.println("What product do you want to purchase (PID)?");
        pId = scanner.nextInt();
        scanner.nextLine();

        rs = runStatementWithResultSet("select * from " +
                PRODUCT_TABLE + " where p_id = \'" + pId + "\'");

        if (!rs.next()) {
            System.out.println("Invalid product id, quitting back to menu...");
            return;
        }
        String brand, name;
        brand = rs.getString("brand");
        name = rs.getString("p_name");

        System.out.println("\nVendors with " + brand + " " + name + " in their inventory: ");

        String stmt = "select v_id vendor, brand, p_name, p_size, price, quantity " +
                "from " + PRODUCT_TABLE + " natural join " + VENDOR_INVENTORY_TABLE +
                " where p_id = \'" + pId + "\'";

        rs = runStatementWithResultSet(stmt);
        printMetaData(rs);
        if (!viewInfo(rs)) {
            System.out.println("There are no vendors with " + brand + " " + name + " currently in stock.");
            return;
        }

        System.out.println("Which vendor would you like to purchase from?");
        firstRun = true;
        vId = -1;
        while (!checkExists(PRODUCT_TABLE + " natural join " + VENDOR_INVENTORY_TABLE,
                "v_id", vId)) {
            if (!firstRun) {
                System.out.println("Invalid id, please try again");
            }
            firstRun = false;
            vId = scanner.nextInt();
            scanner.nextLine();
        }

        //add to vendor_pay_for table
        rs = runStatementWithResultSet("select max(vo_id) from " + VENDOR_PAY_TABLE);
        rs.next();
        voId = rs.getInt(1);
        voId++;

        rs = runStatementWithResultSet("select * from " + VENDOR_INVENTORY_TABLE);
        rs.next();
        double price = rs.getDouble("price");

        System.out.println("How many units would you like to purchase?");
        int quantity = scanner.nextInt();
        scanner.nextLine();

        rs = runStatementWithResultSet("select price, quantity from " + VENDOR_INVENTORY_TABLE + " where p_id = " + pId +
                " and v_id = " + vId);
        rs.next();
        int newQuantity = rs.getInt("quantity") - quantity;

        quantity = verifyQuantity(quantity, newQuantity);

        runStatement("insert into " + VENDOR_PAY_TABLE + " (vo_id, v_id, loc_id) VALUES (" +
                voId + ", " + vId + ", " + locId + ")");

        //add to vendor order
        runStatement("insert into " + VENDOR_ORDER_TABLE + " (vo_id, p_id, v_id, price, quantity) VALUES (" +
                voId + ", " + pId + ", " + vId + ", " + price + ", " + quantity + ")");

        checkMinQuantity();

        System.out.println("Add more items to your order (y/n)?");
        String more = scanner.nextLine();
        if (more.toLowerCase().trim().equals("y")) {
            addItemToVendorOrder();
        } else {
            //TODO: end order
        }

    }

    private static void viewCurrentOrder() throws SQLException {
        viewInfo("*", VENDOR_ORDER_TABLE, "where vo_id = " + voId);
    }

    private static void addItemToVendorOrder() throws SQLException {
        viewInfo("*", PRODUCT_TABLE + " natural join " + VENDOR_INVENTORY_TABLE,
                "where v_id = \'" + vId + "\' order by p_id");
        System.out.println("What product do you want to purchase (PID) from vendor " + vId + "?");
        pId = scanner.nextInt();
        scanner.nextLine();

        rs = runStatementWithResultSet("select * from " +
                PRODUCT_TABLE + " natural join " + VENDOR_PAY_TABLE + " where p_id = \'" +
                pId + "\' and v_id = \'" + vId + "\'");

        if (!rs.next()) {
            System.out.println("Invalid product id");
            addItemToVendorOrder();
        }

        rs = runStatementWithResultSet("select * from " + VENDOR_INVENTORY_TABLE);
        rs.next();
        double price = rs.getDouble("price");

        System.out.println("How many units would you like to purchase?");
        int quantity = scanner.nextInt();
        scanner.nextLine();

        rs = runStatementWithResultSet("select price, quantity from " + VENDOR_INVENTORY_TABLE + " where p_id = " + pId +
                " and v_id = " + vId);
        rs.next();
        int newQuantity = rs.getInt("quantity") - quantity;

        quantity = verifyQuantity(quantity, newQuantity);

//        if (firstItem) {
//            runStatement("insert into " + VENDOR_PAY_TABLE + " (vo_id, v_id, loc_id) VALUES (" +
//                    voId + ", " + vId + ", " + locId + ")");
//            firstItem = false;
//        }
        //add to vendor order
        runStatement("insert into " + VENDOR_ORDER_TABLE + " (vo_id, p_id, v_id, price, quantity) VALUES (" +
                voId + ", " + pId + ", " + vId + ", " + price + ", " + quantity + ")");

        System.out.println("Add more items to your order (y/n)?");
        String more = scanner.nextLine();
        if (more.toLowerCase().trim().equals("y")) {
            addItemToVendorOrder();
        } else {
            checkout();
        }
    }

    private static void checkout() throws SQLException {
        runStatement("select * from " + VENDOR_ORDER_TABLE + " where vo_id = \'" +
                voId + "\'");
        System.out.println("Would you like this to be a repeating order (y/n)?");
        String repeat = scanner.nextLine().toLowerCase().trim();
        if (repeat.equals("y")) {
            System.out.println("At what interval (in number of weeks)?");
            int periodic = scanner.nextInt();
            scanner.nextLine();
            runStatement("insert into " + PERIODIC_TABLE + " (vo_id, freq) VALUES " +
                    "(" + voId + ", " + periodic + ")");
            System.out.println("You will get a shipment of this order every " +
                    periodic + " weeks.");
        }
        System.out.println("Thanks for your business!");
        System.exit(0);
    }

    private static int verifyQuantity(int quantity, int newQuantity) throws SQLException {
        while (newQuantity < 0) {
            //Trying to buy more than what's available
            System.out.println("Only have " + rs.getInt("quantity") + " available. Enter Quantity:");
            quantity = scanner.nextInt();
            scanner.nextLine();
            rs = runStatementWithResultSet("select price, quantity from " + VENDOR_INVENTORY_TABLE + " where p_id = " + pId +
                    " and v_id = " + vId);
            if (rs.next()) {
                newQuantity = rs.getInt("quantity") - quantity;
            }
        }

        updateColumn(VENDOR_INVENTORY_TABLE, "quantity", newQuantity, "v_id = " + vId);

        rs = runStatementWithResultSet("select quantity from " + LOCATION_INVENTORY_TABLE + " where" +
                " loc_id = " + locId + " and p_id = " + pId);
        int newLocQuantity;
        if (rs.next()) {
            newLocQuantity = quantity + rs.getInt("quantity");
            updateColumn(LOCATION_INVENTORY_TABLE, "quantity", newLocQuantity, "loc_id = " + locId);
        } else {
            System.out.println("How much will you be selling this product for?");
            double price = scanner.nextDouble();
            scanner.nextLine();
            runStatement("insert into " + LOCATION_INVENTORY_TABLE + " (p_id, loc_id, price, quantity) VALUES (" +
                    pId + ", " + locId + ", " + price + ", " + quantity + ")");
        }
        return quantity;
    }


    private static void selectStore() throws SQLException {
        //if valid id chosen, skips to end of the method
        while (!checkExists(LOCATION_TABLE, "loc_id", locId)) {
            boolean initial = true;
            while (!checkExists(LOCATION_TABLE, "loc_id", locId + "")) {
                viewAllInfo(LOCATION_TABLE);
                if (initial) {
                    System.out.println("Select your store (enter store ID):");
                } else {
                    System.out.println("Invalid store ID, try again:");
                }
                locId = scanner.nextInt();
                scanner.nextLine();
                initial = false;
                if (locId == -1)
                    return;
            }
            System.out.println("Welcome! Store id: " + locId);
        }
    }

    private static void selectVendor() throws SQLException {
        //if valid id chosen, skips to end of the method
        while (!checkExists(VENDOR_TABLE, "v_id", vId)) {
            boolean initial = true;
            while (!checkExists(VENDOR_TABLE, "v_id", vId + "")) {
                viewAllInfo(VENDOR_TABLE);
                if (initial) {
                    System.out.println("Select a vendor to order from (enter vendor ID):");
                } else {
                    System.out.println("Invalid vendor ID, try again:");
                }
                vId = scanner.nextInt();
                scanner.nextLine();
                initial = false;
            }
        }
    }

    private void setMinQuantity() {
        System.out.print("Choose new alert quantity: ");
        minQuantity = scanner.nextInt();
        scanner.nextLine();
        System.out.println();
        if (minQuantity < 0) {
            System.out.println("Cannot select a minimum quantity less than 0. Defaulting to 0");
            minQuantity = 0;
        }
    }

    private static void checkMinQuantity() throws SQLException {
        rs = runStatementWithResultSet("select * from " + LOCATION_INVENTORY_TABLE + " where loc_id = \'" +
                locId + "\' and quantity <= " + minQuantity);

        if (rs.next()) {
            System.out.println("ALERT!! Your quantity has fallen under your specified amount for these items: ");
            viewInfo(rs);
        }
    }

    private static void checkValidStore() throws SQLException {
        if (!checkExists(LOCATION_TABLE, "loc_id", locId)) {
            selectStore();
        }
    }

    private static void checkValidVendor() throws SQLException {
        if (!checkExists(VENDOR_TABLE, "v_id", vId)) {
            selectVendor();
        }
    }

    public static void main(String[] args) {
        try {
            (new OrderingFromVendorsUI()).runInterface();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
