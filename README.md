# README #
All interfaces use the same initialization function. This prompts the user for their username and password. It loops until it successfully connects to the database.


Customer Ordering Interface:
* The interface starts by asking the user to select a store to shop at. They have the option of selecting the online store or they can view all the store locations and select one from that list.
* Then the user is asked if they are a member. If so, they are asked for their member id. If there's an invalid id, it prompts for it again.
* If the user has chosen to shop not online, they are prompted to select a store id (listed)
* The user is then shown a menu where they have a variety of options to choose from.
* They can choose to add an item to their cart, view the current inventory in the store they're in, view their cart, or remove an item from their cart.
* Adding an item prompts the user for the ID, then prompts for the quantity the user wants to purchase. Removing from the cart works the same way.
* When the user is satisfied with their purchase, they can checkout. At checkout, they are prompted for a payment method. They pay and their transaction is complete.

Member Management Interface:
* This would mostly be used for a customer service person helping a customer
* They have the option to view all members to sort through them manually
* They can update a member's information such as address and name
* They can see a member's order history
* They can view a member's profile
* Or they can view the members in a custom order (to find by last name, etc)

Vendor Ordering Interface:
* The user can select/switch which store location they are ordering from
* The user can view the inventory of their store (prompts them to select a store if they have not already)
* The user can set an alert so they are notified if any of their products fall under a certain quantity
* The user can start/make an order from a vendor for their store 